from cryptography.hazmat.primitives.ciphers.aead import (
    AESGCM,
)


def encrypt(msg: bytes, key: bytes, nonce: bytes) -> bytes:
    aes = AESGCM(key)
    encr_msg = aes.encrypt(nonce, msg, None)
    return encr_msg


def decrypt(msg: bytes, key: bytes, nonce: bytes) -> bytes:
    aes = AESGCM(key)
    decr_msg = aes.decrypt(nonce, msg, None)
    return decr_msg
