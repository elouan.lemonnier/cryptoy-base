from math import (
    gcd,
)

from cryptoy.utils import (
    draw_random_prime,
    int_to_str,
    modular_inverse,
    pow_mod,
    str_to_int,
)


def keygen() -> dict:
    e = 65537
    p = draw_random_prime()
    q = draw_random_prime()
    d = modular_inverse(e, (p-1)*(q-1))

    dict_key = {"public_key": (e, p * q), "private_key": d}
    return dict_key


def encrypt(msg: str, public_key: tuple) -> int:
    int_msg = str_to_int(msg)
    if int_msg >= public_key[1]:
        raise ValueError("message trop long")
    encrypted_msg = pow_mod(int_msg, public_key[0], public_key[1])
    
    return encrypted_msg

def decrypt(msg: int, key: dict) -> str:
    private_key = key["private_key"]
    decrypted_msg = pow_mod(msg, private_key, key["public_key"][1])
    decr_msg = int_to_str(decrypted_msg)
    
    return decr_msg
