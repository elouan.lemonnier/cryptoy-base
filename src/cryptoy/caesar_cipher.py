from cryptoy.utils import (
    str_to_unicodes,
    unicodes_to_str,
)

# TP: Chiffrement de César


def encrypt(msg: str, shift: int) -> str:
    encrypted_msg = ""
    for char in msg:
        uni_char = ord(char)
        encrypted_uni_char = (uni_char + shift) % 0x110000
        encrypted_char = chr(encrypted_uni_char)
        encrypted_msg += encrypted_char
    return encrypted_msg


def decrypt(encrypted_msg: str, shift: int) -> str:
    decrypted_msg = ""
    for char in encrypted_msg:
        uni_char = ord(char)
        decrypted_uni_char = (uni_char - shift) % 0x110000
        decrypted_char = chr(decrypted_uni_char)
        decrypted_msg += decrypted_char
    return decrypted_msg


def attack() -> tuple[str, int]:
    s = "恱恪恸急恪恳恳恪恲恮恸急恦恹恹恦恶恺恪恷恴恳恸急恵恦恷急恱恪急恳恴恷恩怱急恲恮恳恪恿急恱恦急恿恴恳恪"
    
    for shift in range(0x110000):
        decrypted_msg = ""
        for char in s:
            uni_char = ord(char)
            decrypted_uni_char = (uni_char - shift) % 0x110000
            decrypted_char = chr(decrypted_uni_char)
            decrypted_msg += decrypted_char
        
        if "ennemis" in decrypted_msg:
            return decrypted_msg, shift

    raise RuntimeError("Failed to attack")