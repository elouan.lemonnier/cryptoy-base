from math import (
    gcd,
)

from cryptoy.utils import (
    str_to_unicodes,
    unicodes_to_str,
)

# TP: Chiffrement affine


def compute_permutation(a: int, b: int, n: int) -> list[int]:
    result = [0]*n
    
    for i in range(n-1):
        
        result[i] = a*i+b
        result[i] %= n      
    return result


def compute_inverse_permutation(a: int, b: int, n: int) -> list[int]:
    perm = compute_permutation(a, b, n)
    result = [0] * n

    for i in range(n):
        result[perm[i]] = i
    
    return result


def encrypt(msg: str, a: int, b: int) -> str:
    unicodes = str_to_unicodes(msg)  # Convert the message to a list of Unicode code points
    encrypted_unicodes = []
    perm = compute_permutation(a, b, 0x110000)
    for unicode_val in unicodes:
        encrypted_unicode_val = perm[unicode_val]# Apply the affine cipher formula
        encrypted_unicodes.append(encrypted_unicode_val)

    encrypted_msg = unicodes_to_str(encrypted_unicodes)  # Convert the encrypted code points back to string

    return encrypted_msg

def encrypt_optimized(msg: str, a: int, b: int) -> str:
    unicodes = str_to_unicodes(msg)  # Convert the message to a list of Unicode code points
    encrypted_unicodes = []
    perm = compute_permutation(a, b, 0x110000)
    for unicode_val in unicodes:
        encrypted_unicode_val = (a * unicode_val + b) % 0x110000# Apply the affine cipher formula
        encrypted_unicodes.append(encrypted_unicode_val)

    encrypted_msg = unicodes_to_str(encrypted_unicodes)  # Convert the encrypted code points back to string

    return encrypted_msg



def decrypt(msg: str, a_inverse: int, b: int) -> str:
    unicodes = str_to_unicodes(msg)  # Convert the message to a list of Unicode code points
    decrypted_unicodes = []
    perm = compute_inverse_permutation(a_inverse, b, 0x110000)
    for unicode_val in unicodes:
        decrypted_unicode_val = perm[unicode_val]# Apply the affine cipher formula
        decrypted_unicodes.append(decrypted_unicode_val)

    decrypted_msg = unicodes_to_str(decrypted_unicodes)  # Convert the encrypted code points back to string
    
    return decrypted_msg


def decrypt_optimized(msg: str, a_inverse: int, b: int) -> str:
    unicodes = str_to_unicodes(msg)  # Convert the message to a list of Unicode code points
    decrypted_unicodes = []
    
    for unicode_val in unicodes:
        decrypted_unicode_val = (a_inverse * (unicode_val - b)) % 0x110000   # Apply the inverse affine cipher formula
        decrypted_unicodes.append(decrypted_unicode_val)

    decrypted_msg = unicodes_to_str(decrypted_unicodes)  # Convert the decrypted code points back to string
    return decrypted_msg


def compute_affine_keys(n: int) -> list[int]:
    keys = []
    
    for a in range(1, n):
        if gcd(a, n) == 1:
            keys.append(a)
    
    return keys


def compute_affine_key_inverse(a: int, affine_keys: list, n: int) -> int:
    for a_1 in affine_keys:
        if (a * a_1) % n == 1:
            return a_1
    raise RuntimeError(f"{a} has no inverse")


def attack() -> tuple[str, tuple[int, int]]:
    s = "࠾ੵΚઐ௯ஹઐૡΚૡೢఊஞ௯\u0c5bૡీੵΚ៚Κஞїᣍફ௯ஞૡΚր\u05ecՊՊΚஞૡΚՊեԯՊ؇ԯրՊրր"
    # trouver msg, a et b tel que affine_cipher_encrypt(msg, a, b) == s
    # avec comme info: "bombe" in msg et b == 58
    for a in range(0x110000):
        msg = decrypt(s, a, 58)
        if "bombe" in msg:
            tuple_ret = a, 58
            return msg, tuple_ret

    raise RuntimeError("Failed to attack")


def attack_optimized() -> tuple[str, tuple[int, int]]:
    s = (
        "જഏ൮ൈ\u0c51ܲ೩\u0c51൛൛అ౷\u0c51ܲഢൈᘝఫᘝా\u0c51\u0cfc൮ܲఅܲᘝ൮ᘝܲాᘝఫಊಝ"
        "\u0c64\u0c64ൈᘝࠖܲೖఅܲఘഏ೩ఘ\u0c51ܲ\u0c51൛൮ܲఅ\u0cfc\u0cfcඁೖᘝ\u0c51"
    )
    target_substring = "bombe"
    aff_k = compute_affine_keys(0x110000)
    for a in aff_k:
        a_inverse = compute_affine_key_inverse(a, aff_k, 0x110000)
        print("AAAAAAAAa",a_inverse)

        for b in range(1, 10000):
            decrypted_msg = decrypt_optimized(s, a_inverse, b)
            if target_substring in decrypted_msg:
                return decrypted_msg, (a, b)

    raise RuntimeError("Failed to attack")