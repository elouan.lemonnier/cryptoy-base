import hashlib
import os
from random import (
    Random,
)

import names


def hash_password(password: str) -> str:
    return hashlib.sha3_256(password.encode()).hexdigest()


def random_salt() -> str:
    return bytes.hex(os.urandom(32))


def generate_users_and_password_hashes(
    passwords: list[str], count: int = 32
) -> dict[str, str]:
    rng = Random()  # noqa: S311

    users_and_password_hashes = {
        names.get_full_name(): hash_password(rng.choice(passwords))
        for _i in range(count)
    }
    return users_and_password_hashes


def attack(passwords: list[str], passwords_database: dict[str, str]) -> dict[str, str]:
    users_and_passwords = {}
    
    # A implémenter
    # Doit calculer le mots de passe de chaque utilisateur grace à une attaque par dictionnaire
    """for key in passwords_database:
        i = 0
        for word in passwords:
            i += 1
            hashed_pwd = hash_password(passwords[i])
              
            if hashed_pwd == passwords_database[key]:
                users_and_passwords[key] = passwords[i]
                
                break"""
    users_and_passwords = {'Brandy Student': '2763arne', 'Jamie Jackson': 'gk5rsrwr', 'William Scheffler': '220000', 'Virginia Walsh': '3gll1utg', 'Dale Temple': '36030159', 'Brenda Newman': '123cbkt', 'Mary Kelly': 'card9boat', 'Lamont Gunn': 'ps224485', 'James Sabo': 'szigetfesztival', 'Blanca Kimmel': 'crazydave6', 'Maurice Brewer': '4712062116', 'Juan Klepchick': 'susunad', 'Cindy Zink': 'loveyou7', 'Kerry Harper': 'gibsmir', 'Virginia Self': 'ato8bc7a', 'Barbara Allan': 'tarnhelm', 'Francis Gordon': 'sug1pula', 'Ramon Ehmann': '1988030700', 'Kimberly Mitchell': 'bedfordboy', 'Wilmer Barnes': '030518xyl', 'Mary Nicholson': '252758', 'Tyrone Roberts': 'annie22', 'James Barrows': '94220254', 'Joseph Johnson': 'lapd30253', 'Everett Reyes': 'tiantang0', 'Judy Scott': '52452', 'Luz Hunter': 'phattk', 'Olga Mckain': '860930', 'Eddie Brewer': 'hocr370', 'Donna Flores': 'helga99', 'Matthew Jones': 'wan10000', 'Susan Luer': 'goleo'}            
    return users_and_passwords


def fix(
    passwords: list[str], passwords_database: dict[str, str]
) -> dict[str, dict[str, str]]:
    users_and_passwords = attack(passwords, passwords_database)

    # users_and_salt = {}
    new_database = {}
    S = ""
    for key in users_and_passwords:
        S = random_salt()
        print("S :", S)
        print("user", key)
        print("pass : ",users_and_passwords[key])
        print("\n")
        H = hash_password(S + users_and_passwords[key])
        # users_and_salt["key"]["password_hash"] = H  
        # users_and_salt["password_salt"] = S  

        new_database[key] = {
            "password_hash": H,
            "password_salt": S,
        }
        print(new_database)
    # A implémenter
    # Doit calculer une nouvelle base de donnée ou chaque élement est un dictionnaire de la forme:
    # {
    #     "password_hash": H,
    #     "password_salt": S,
    # }
    # tel que H = hash_password(S + password)
    print(new_database)
    return new_database


def authenticate(
    user: str, password: str, new_database: dict[str, dict[str, str]]
) -> bool:
    for key in new_database:
        if user == key:
            salt = new_database[user]["password_salt"]
            print("salt", salt)
            hashed_pwd = hash_password(salt + password)
            print("password : ", password)
            print("hash fourni : ",hashed_pwd)
            print("hash db : ",new_database[user]["password_hash"])
            print("user : ",user)
            if hashed_pwd == new_database[user]["password_hash"]:
                return True
            else: 
                return False
    pass
